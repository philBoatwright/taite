package com.taite.Listener;

import com.taite.Util.ScreenshotUtil;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * User: pboatwright
 * Date: 4/8/13
 * Time: 8:44 AM
 */
public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestStart(ITestResult result) {

        super.onTestStart(result);

        String browserUnderTest = "defaultBrowser";
        String testContextAppVersion = (String) result.getTestContext().getAttribute("appVersion");

        // Set current browser under test if other than default
        if (StringUtils.isNotBlank(result.getTestContext().getCurrentXmlTest().getParameter("browser"))) {
            browserUnderTest = result.getTestContext().getCurrentXmlTest().getParameter("browser");
        }

        // If the current test method is assigned a version compare with current app version before executing
        if (StringUtils.isNotBlank(testContextAppVersion)) {

            // Remove "SNAPSHOT" or other sub version information from current app version
            String currentAppVersion;
            if (testContextAppVersion.contains("-")) {

                String[] testConextAppVersionPieces = testContextAppVersion.split("-");
                currentAppVersion = testConextAppVersionPieces[0];

            } else {


                currentAppVersion = testContextAppVersion;

            }

            // Check if current test method is assigned a version group
            String[] groups = result.getMethod().getGroups();
            String currentTestVersion = null;
            for (String group : groups) {

                if (group.contains("version")) {

                    currentTestVersion = group.replace("version-", "");

                }

            }

            if (StringUtils.isNotBlank(currentTestVersion)) {

                // Set app version by removing all periods to make a single integer
                Integer appVersion = Integer.parseInt(currentAppVersion.replace(".", ""));
                if (StringUtils.length(appVersion.toString()) <= 3) {
                    String newAppVersion = appVersion.toString() + "0";
                    appVersion = Integer.parseInt(newAppVersion);
                }

                // Set current test method's assigned version by removing all periods and making a single integer
                Integer testVersion = Integer.parseInt(currentTestVersion.replace(".", ""));

                // If test method's version is less than or equal to the current app version go ahead and execute
                if (testVersion <= appVersion) {

                    System.out.print(">>>>>> Test: " + result.getName() + " [" + browserUnderTest + "]...");

                }
                // If the assigned test method's version is greater than the current app version skip it
                else {

                    System.out.println(">>>>>> SKIPPED TEST - OUT OF SCOPE: " + result.getName() + " [" + testVersion + "]...");
                    // throw new SkipException("SKIPPED TEST - OUT OF SCOPE (It's version group [" + testVersion + "] is greater than current App Version [" + appVersion + "])");

                }

            }
            // If not assigned a version group then go ahead and execute
            else {

                System.out.print(">>>>>> Test: " + result.getName() + " [" + browserUnderTest + "]...");

            }

        }
        // If no app version variable has been set then go ahead and execute
        else {

            System.out.print(">>>>>> Test: " + result.getName() + " [" + browserUnderTest + "]...");

        }


    }

    @Override
    public void onTestSuccess(ITestResult result) {
        String browserUnderTest = (String)result.getTestContext().getAttribute("browser");
        super.onTestSuccess(result);
        System.out.print("PASSED Test: " + result.getName() + " [" + browserUnderTest + "]");

    }

    @Override
    public void onTestFailure(ITestResult result) {
        String browserUnderTest = (String)result.getTestContext().getAttribute("browser");
        super.onTestFailure(result);
        System.out.println("FAILED...");
        WebDriver driver = (WebDriver) result.getTestContext().getAttribute("webDriver");
        if (driver != null) {
            ScreenshotUtil.takeScreenshot(driver);
        }

    }
}
