package com.taite.Listener;

import com.taite.Listener.WebDriverEventAdapter;
import org.openqa.selenium.WebDriver;

/**
 * User: pboatwright
 * Date: 4/8/13
 * Time: 9:31 AM
 */
public class DriverEventListener extends WebDriverEventAdapter {

    public DriverEventListener(WebDriver webDriver) {
        super(webDriver);
    }

}
