package com.taite.Util;

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.cropper.indent.IndentCropper;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import static ru.yandex.qatools.ashot.cropper.indent.IndentFilerFactory.blur;

/**
 * User: pboatwright
         * Date: 4/22/13
         * Time: 9:19 AM
         */

public class WebDriverUtil {

    private boolean ACCEPT_NEXT_ALERT = true;
    private String DOWNLOAD_FILE_PATH = "";
    private String CURRENT_BROWSER;

    private EventFiringWebDriver driver;
    private FluentWait wait;
    private WebElement element;
    private Robot robot;
    private Properties autProps;
    private Properties cleanupProps;
    private static Resource autResource;
    private static Resource cleanupResource;

    public WebDriverUtil(EventFiringWebDriver driver, FluentWait wait, Properties appPropertiesFile) throws Exception{

        this.driver = driver;
        this.autProps=appPropertiesFile;
        this.robot = new Robot();
        this.wait = wait;
        this.CURRENT_BROWSER = null; //extractBrowserName(this.driver.executeScript("return navigator.userAgent", ""));
        if ( cleanupResource == null )
            cleanupResource = new ClassPathResource( "/cleanup.properties" );
        try {
            this.cleanupProps = PropertiesLoaderUtils.loadProperties(cleanupResource);
        }
        catch (IOException exception) {
//            System.out.println("Failed to load the Cleanup Properties file");
        }

    }

    public String getCleanupValue(String key) {

        return cleanupProps.getProperty(key);

    }
    public void setCleanupValue(String key, String value) {

        cleanupProps.setProperty(key,value);

    }
    public String getLocatorValue(String key) {
        wait(1000);
        return autProps.getProperty(key);
    }
    public void replaceLocIdentifierXpath(String locator){

        String locatorXpath = getLocatorValue(locator);
        autProps.setProperty("app.locIdentifier", locatorXpath);

    }
    public void setToExpectedOption(String key,String firstReplacenent,String secondReplacement,boolean reset){
        setToExpectedOption(key,firstReplacenent,reset);
        setToExpectedOption(key,secondReplacement,reset);
    }

    public void setToExpectedOption(String key, String replacementData, boolean reset){

        String xpath = getLocatorValue(key);

        if (reset) {
            xpath=StringUtils.replaceOnce(xpath,replacementData,"option");

//            xpath = xpath.replace(replacementData,"option");

        }
        else{

            xpath=StringUtils.replaceOnce(xpath,"option",replacementData);
//            xpath = xpath.replace("option", replacementData);

        }
//        System.out.println("Debug: "+xpath);

        autProps.setProperty(key, xpath);
//        System.out.println("XPATH IS: "+xpath);

    }

    private static String extractBrowserName(String userAgent) {

        UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
        ReadableUserAgent agent = parser.parse(userAgent);
        return agent.getName();

    }


    public void getUrl(String url)
    {
        driver.get(url);
    }

    public static ExpectedCondition<Boolean> titleToLowerContains(final String title) {

        return new ExpectedCondition<Boolean>() {

            private String currentTitle = "";

            public Boolean apply(WebDriver driver) {

                currentTitle = driver.getTitle().toLowerCase();
                return currentTitle != null && currentTitle.contains(title);

            }

            public String toString() {
                return String.format("title to contain \"%s\". Current title: \"%s\"", title, currentTitle);
            }

        };

    }

    public static WebElement waitForElementById(Wait<WebDriver> wait, final String elementId) {

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(By.id(elementId));
            }
        });

        return element;

    }

    public static String spinnerWait(WebDriverWait spinnerWait, String spinnerId) {

        String result = "success";

        try {

            if (spinnerWait.withTimeout(5, TimeUnit.SECONDS).until(ExpectedConditions.presenceOfElementLocated((By.id(spinnerId)))).isDisplayed()) {

                spinnerWait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(spinnerId)));

            }

        }catch( Exception ex ){}
        return result;

    }

    public By getByWithLocatorKey(String key) throws Exception {
        String locator = autProps.getProperty(key);
        String locatorType = locator.split("\\|")[0];
        String locatorValue = locator.split("\\|")[1];
        switch (locatorType) {
            case "id":
                return By.id(locatorValue);
            case "name":
                return By.name(locatorValue);
            case "class":
                return By.className(locatorValue);
            case "tag":
                return By.tagName(locatorValue);
            case "partiallinktext":
                return By.partialLinkText(locatorValue);
            case "linktext":
                return By.linkText(locatorValue);
            case "css":
                return By.cssSelector(locatorValue);
            case "xpath":
                return By.xpath(locatorValue);
            default:
                throw new Exception("Unknown locator type '" + locatorType + "'");
        }
    }

    public WebElement getWebElementWithLocatorKey (String locator){

        try {
           element=null;
           element = driver.findElement(getByWithLocatorKey(locator));
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return element;
    }

    public WebElement getWebElementByXpath(String xpath)
    {
        return driver.findElement(By.xpath(xpath));
    }

    public String decodeBase64(String value) throws IllegalArgumentException {

        byte[] valueDecoded= Base64.decodeBase64(value);
        return new String(valueDecoded);

    }

    public WebElement get(){
        return this.element;
    }

    public String getCSSPropertyValue(String selector,String property, String locatorName){
        return getCSSPropertyValue(selector,property,getWebElement(locatorName));
    }

    public String getCSSPropertyValue(String selector,String property, WebElement we){
        return executeJavaScript("return window.getComputedStyle(arguments[0], '"+selector+"').getPropertyValue('"+property+"');",we).toString();
    }

    public String getDOWNLOAD_FILE_PATH(){
        return this.DOWNLOAD_FILE_PATH;
    }

    public void setDOWNLOAD_FILE_PATH(String DOWNLOAD_FILE_PATH)
    {
        this.DOWNLOAD_FILE_PATH = DOWNLOAD_FILE_PATH;
    }

    public void setBrowserSize(int width, int height) {

        Dimension dim = new Dimension(width,height);
        driver.manage().window().setSize(dim);

    }

    public boolean isElementPresent(String locatorName) {

        try
        {
            this.getWebElementWithLocatorKey(locatorName).isDisplayed();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public void goToURL(String locatorName){

        String go =this.getLocatorValue(locatorName);

    }

    public void close() {

        try
        {
            // Chrome requires Quit() or else the command window is left open
            //           System.out.println("**********************  CLOSING BROWSER  ************************************");
            driver.quit();
            driver.close();
        }
        catch (Exception e)
        {
            try { driver.close(); }
            catch (Exception e1) {
                //  core.AddAssertHandler("Instance failed to close: " + e.getMessage());
            }
        }

    }

    public boolean alertPresent() {

        try
        {
            driver.switchTo().alert();
        }
        catch (NoAlertPresentException e)
        {
            return false;
        }

        return true;

    }

    public void confirmAlert()
    {
        driver.switchTo().alert().accept();
    }

    public void alertAccept()
    {
        driver.switchTo().alert().accept();
    }

    public void dismissAlert()
    {
        driver.switchTo().alert().dismiss();
    }

    public void alertSendKeys(String keys)
    {
        driver.switchTo().alert().sendKeys(keys);
    }

    public WebElement webElement(String locatorName)
    {
        return this.getWebElementWithLocatorKey(locatorName);
    }

    public void refresh()
    {
        driver.navigate().refresh();
    }

    public void clear(String locatorName)
    {

        try
        {
            element = getWebElementWithLocatorKey(locatorName);
            element.clear();
        }
        catch (Exception e) { }

    }

    public void doubleClick(String locatorName)  {

        doubleClick(this.getWebElementWithLocatorKey(locatorName));
    }

    public void doubleClick(WebElement we) {

        Actions firstclicks = new Actions(driver);

        Action doubleclick1 = firstclicks.doubleClick(we).build();

        doubleclick1.perform();

    }

    public void tripleClick(WebElement we) {

        Actions tripleClick = new Actions(driver);

        Action tripleclick = tripleClick.doubleClick(we).click(we).build();

        tripleclick.perform();

    }

    public void tripleClick(String locatorName) {

        tripleClick(this.getWebElementWithLocatorKey(locatorName));

    }

    public void tripleClickIt(String locatorName) {

       doubleClick(locatorName);
       click(locatorName);
    }


    public void click(String locatorName){

        this.getWebElementWithLocatorKey(locatorName).click();

    }

    public void click(WebElement we){

        we.click();

    }

    public void clickWhileExists(String locatorName) {

        boolean notClicked=true;
        int i=0;
        try {
            if (waitForElementPresent(locatorName,60)) {
                while (notClicked) {
                    if (i == 10) {
                        throw new Exception(locatorName + " not clickable.");
                    }
                    this.click(locatorName);
                    notClicked=isElementPresent(locatorName);
                    i++;
                }
            }else return;
        } catch (Exception e) {
            wait(1000);
            i++;
        }

    }

    public void waitAndClick(String locatorName){

        waitForElementPresent(locatorName,5);
        this.click(locatorName);

    }

    public void clickAt(String locatorName, int xOffset, int yOffset) throws Exception {

        WebElement we= this.getWebElementWithLocatorKey(locatorName);
        Actions builder = new Actions(driver);
        builder.moveToElement(we).moveByOffset(xOffset, yOffset).click().build().perform();

    }

    public void focus(WebElement we) {

        new Actions(driver).moveToElement(we).perform();
        try {
            we.click();
        }
        catch( Exception e ){}

    }

    public void focus(String locatorName)  {

        element = this.getWebElementWithLocatorKey(locatorName);
        focus(element);

    }
    //To be used when clicking element throws an error, i.e. Element is not clickable at point ...
    public void   clickWhenClickable(String locatorName, int scrollPixels){
         if (waitForElementPresent(locatorName, 3)) {
            for (int i = 0; i < 5; i++) {
                try {
                    click(locatorName);
                    return;
                } catch (Exception e) {
                    try {
                        Thread.sleep(1000);
                        if (scrollPixels>0){
                            scrollPixels=scrollPixels*(i+1);
                            scrollDownJS(Integer.toString(scrollPixels));
                        }
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    if (i == 4)
                        System.out.println(e.getMessage());
                }
            }
        }
    }

    public boolean isChecked(String locatorName) {

        try {
            return this.getWebElementWithLocatorKey(locatorName).isSelected();
        }
        catch(Exception e) {
            return false;
        }

    }

    public boolean waitUntilNotVisable(String locatorName) {
        String xpath="";
        try {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(getLocatorValue(locatorName))));
            return !element.isDisplayed();
        }
        catch (Exception x) { return false; }

    }

    public boolean waitUntilVisible(String locatorName) {
        boolean retVal=false;
        try {
            element = this.getWebElementWithLocatorKey(locatorName);
            if (element!=null) {
                wait.until(ExpectedConditions.visibilityOf(element));
                retVal = element.isDisplayed();
            }
            return retVal;
        }
        catch (Exception x) { return false; }

    }

    public boolean isVisableClick(String locatorName) {

        try {
            if (waitUntilVisible(locatorName)) {
                element.click();
                return true;
            }
            else
                return false;
        }
        catch (Exception x) { return false; }


    }

    public boolean isDisplayed(String locatorName) {

        try {
            return this.getWebElementWithLocatorKey(locatorName).isDisplayed();
        }
        catch(Exception e) {
            return false;
        }

    }

    public void selectIndex(String locatorName, int i) {

        Select selector = new Select(this.getWebElementWithLocatorKey(locatorName));
        selector.selectByIndex(i);

    }

    public void sendKeysListBox(String locatorName, String text) throws Exception {

        int i = 0;
        sendkeys(locatorName, text);
        while (!getSelectedText(locatorName).equals(text))
        {
            sendkeys(locatorName, text);
            waitForTextToContain(locatorName, text, 1);
            i++;
            if (i == 15)
                break;
        }

    }

    public void sendKeysRetry(String locatorName, String text,int iAttempts) {

        int i = 0;
        while (! getAttribute(locatorName, "value").equals(text))
        {
            try {

                sendkeys(locatorName, text);
                if (getAttribute(locatorName, "value").equals(text))
                    break;
                wait(1000);
                selectAllText(locatorName);
                i++;
                if (i > iAttempts)
                    break;
            }
            catch(Exception e){i++;}}

    }

    public void sendKeysJS(WebElement we, String text) {
        this.driver.executeScript("arguments[0].value='"+text+"';", new Object[]{we});
    }

    public void sendKeysJS(String locatorName, String text) {
        WebElement we=this.getWebElementWithLocatorKey(locatorName);
        this.driver.executeScript("arguments[0].value='"+text+"';", new Object[]{we});
    }

    public String getInput(String locatorName) throws Exception {

        return getAttribute(locatorName, "value");
    }

    public void selectText(String locatorName, String text)
    {
        selectText(this.getWebElementWithLocatorKey(locatorName), text);
    }

    public void selectText(WebElement we, String text) {

        Select select = new Select(we);
        select.selectByVisibleText(text);

    }

    public void unSelectAll(String locatorName) throws Exception {

        Select selector = new Select(this.getWebElementWithLocatorKey(locatorName));
        selector.deselectAll();

    }

    public void selectValue(String locatorName, String value) {

        Select selector = new Select(this.getWebElementWithLocatorKey(locatorName));
        selector.selectByValue(value);

    }

    public void sendKeysBackSpace(String locatorName, String text)
    {
        String backSpace = Keys.chord(Keys.BACK_SPACE);
        this.getWebElementWithLocatorKey(locatorName).sendKeys(backSpace+text);
    }

    public void sendMultipleBackSpaces(String locatorName, int repeatNum) {

        int i = 0;
        for (i = 0; i < repeatNum; i++)
        {
            sendKeysBackSpace(locatorName, "");
        }

    }


    public void sendKeysTabOut(String locatorName, String text)
    {
        String tab = Keys.chord(Keys.TAB);
        this.getWebElementWithLocatorKey(locatorName).sendKeys(text+tab);
    }

    public void sendkeys(String locatorName, String text)
    {
        this.getWebElementWithLocatorKey(locatorName).sendKeys(text);
    }

    public void sendMultipleKeys(String locatorName, String keys, int repeatNum) {

        int i = 0;
        for (i = 0; i < repeatNum; i++)
        {
            sendKeysEnhanced(locatorName, keys);
        }

    }

    public void ctrlClick(String locatorName) {

        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL).perform();
        click(locatorName);
        action.keyUp(Keys.CONTROL).build().perform();

    }

    public void sendKeysWithLeadingCntrlKey(String locatorName,String text) {

        String enhancedText = Keys.chord(Keys.CONTROL, text);
        sendKeys(locatorName, enhancedText);
    }

    public void sendCtrlC(String locatorName) {

        String copy = Keys.chord(Keys.CONTROL, "c");
        sendKeys(locatorName, copy);

    }

    public void sendCtrlV(String locatorName,String text) {

        String selectAll = Keys.chord(Keys.CONTROL, "v");
        sendKeys(locatorName, selectAll);

    }

    public void sendKeysCopyPaste(String locatorName,String text){

        StringSelection selection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        System.out.println(text);
        sendCtrlV(locatorName,text);

    }

    public void selectAllText(String locatorName) {

        String selectAll = Keys.chord(Keys.CONTROL, "a");
        sendKeys(locatorName, selectAll);

    }

    public void sendENTERKey(String locatorName) {

        String enter = Keys.chord(Keys.ENTER);
        sendKeys(locatorName, enter);

    }

    public void sendDownArrowKey(String locatorName) {

        String down = Keys.chord(Keys.ARROW_DOWN);
        sendKeys(locatorName, down);

    }

    public boolean exists(String locatorName, int timeoutInSeconds) {

        boolean returnVal = false;
        if (timeoutInSeconds >= 0)
        {
            try
            {
                this.getWebElementWithLocatorKey(locatorName).wait(timeoutInSeconds);
                returnVal = true;
            }
            catch (Exception x)
            {
                returnVal = false;
            }
        }
        return returnVal;

    }

    public Object executeJavaScript(String js, WebElement we) {
        Object result=new Object();
        if (we!=null)
        {
            if (driver instanceof JavascriptExecutor)
            {
                result=((JavascriptExecutor)driver).executeScript(js,we);
            }
        }
        else
            result=((JavascriptExecutor)driver).executeScript(js,we);
        return result;
    }

    public String getAttribute(WebElement we, String attribute)
    {
        return we.getAttribute(attribute);
    }

    public String getAttribute(String locatorName, String attribute) {

        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        return getAttribute(we, attribute);

    }

    public boolean attributeExists(WebElement we, String attribute) {

        boolean returnVal = false;
        String val = "";
        try
        {
            val = getAttribute(we, attribute);
            returnVal = !val.equals(null);

        }
        catch (Exception x)
        {
            returnVal = false;
        }
        return returnVal;

    }

    public boolean attributeExists(String locatorName, String attribute) {

        element = this.getWebElementWithLocatorKey(locatorName);
        return attributeExists(element,attribute);

    }

    public List<WebElement> getList(String locatorName) throws Exception {

        List<WebElement> options = null;
        try
        {
            //for select controls only
            exists(locatorName, 15);
            WebElement elem = this.getWebElementWithLocatorKey(locatorName);
            Select selectList = new Select(elem);
            options = selectList.getOptions();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return options;

    }

    public WebElement getByGuid(String locatorName, String guid) {

        WebElement we = null;
        String attrib;
        List<WebElement> WEList = null;
        WebElement returnWe;
        String classPart;
        try
        {
            List<WebElement> elements = driver.findElements(this.getByWithLocatorKey(locatorName));
            for (WebElement e : elements) {
                attrib = e.getAttribute("iDV");
                if (attrib !=null || attrib != "")
                    WEList.add(e);
            }
            we = (WebElement)WEList.toArray()[0];
            attrib = we.getAttribute("id");
            classPart = attrib.substring(attrib.indexOf("_"), 5);
            returnWe = driver.findElement(By.xpath("//*[contains(@id,'" + classPart + "')]/nobr/input"));
            if (returnWe == null)
                returnWe = driver.findElement(By.xpath("//*[contains(@id,'" + classPart + "')]/nobr"));

            return returnWe;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public List<WebElement> getWebElements(String locatorName) {

        try
        {
            List<WebElement> elements = driver.findElements(this.getByWithLocatorKey(locatorName));
            return elements;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public void clickWebElements(String locatorName) {

           List<WebElement> webelements = getWebElements(locatorName);
            for (WebElement we : webelements)
                we.click();

    }

    public void setToFrameWhereElementExists(String locatorName) throws Exception {

        for (int i = 0; i < 4; i++)
        {
            try {
                driver.switchTo().frame(i);
                if (exists(locatorName, 2))
                {
                    System.out.println("Found element at frame : " + i);
                    break;
                }
            }
            catch (Exception e) { System.out.println(e.getMessage()); }
        }

    }

    public void selectMultipleOptions(String locatorName, List<String> oList) {

        waitForElementPresent(locatorName, 5);
        Select select = new Select(this.getWebElementWithLocatorKey(locatorName));
        for (String option : oList)
        {
            try
            {
                select.selectByVisibleText(option);
            }
            catch (Exception x) { }
        }

    }

    public List<String> getSelectedList(String locatorName) throws Exception {

        List<String> rList = null;
        waitForElementPresent(locatorName, 30);
        Select select = new Select(this.getWebElementWithLocatorKey(locatorName));
        List<WebElement> webelements = select.getAllSelectedOptions();
        for (WebElement we : webelements)
            rList.add(we.getText());
        return rList;

    }

    public String getSelectedText(WebElement we) {

        Select select = new Select(we);
        String selectedtext = select.getFirstSelectedOption().getText();
        return selectedtext;

    }

    public boolean waitWhileExists(String locatorName, int iTimeout){

        boolean retVal = false;
        int i = 0;
        while (this.isElementPresent(locatorName))
        {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
            if (i > iTimeout)
            {
                retVal = true;
                break;
            }
        }
        return retVal;

    }

    public boolean waitForElementPresent(String locatorName, int iTimeout) {

        boolean retVal = true;
        int i = 0;
        try
        {
            while (!this.isElementPresent(locatorName))
            {
                Thread.sleep(1000);
                i++;
                if (i == iTimeout)
                {
                    retVal = false;
                    break;
                }
            }
        }
        catch (Exception x) { }
        return retVal;

    }

    public boolean waitForElementVisible(String locatorName, int iTimeout) {

        boolean retVal = true;
        int i = 0;
        try
        {
            while (!this.waitUntilVisible(locatorName))
            {
                Thread.sleep(500);
                i++;
                if (i == iTimeout)
                {
                    retVal = false;
                    break;
                }
            }
        }
        catch (Exception x) { }
        return retVal;

    }

    public void wait(int iTimeoutInMillis) {

        try {
            Thread.sleep(iTimeoutInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public String getSelectedText(String locatorName) throws Exception {

        waitForElementPresent(locatorName, 5);
        return getSelectedText(this.getWebElementWithLocatorKey(locatorName));

    }

    public String getInnerHtml(String locatorName) {

        String returnText="";
        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        return getInnerHtml(we);

    }

    public String getInnerHtml(WebElement we) {

        String returnText="";
        if (driver instanceof JavascriptExecutor)
        {
            returnText= ((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;",we).toString();
        }
        return returnText;

    }

    public String getOuterHtml(WebElement we) {

        String returnText="";
        if (driver instanceof JavascriptExecutor)
        {
            returnText= ((JavascriptExecutor)driver).executeScript("return arguments[0].outerHTML",we).toString();
        }
        return returnText;

    }

    public String getText(String locatorName) {

        String text = "";
        WebElement we;
        waitForElementPresent(locatorName, 5);
        text = this.getWebElementWithLocatorKey(locatorName).getText();
        if (text.isEmpty()) {
            text = this.getAttribute(locatorName, "value");
        }
        if (text==null || text.isEmpty()){
            we = this.getWebElementWithLocatorKey(locatorName);
            text = getInnerHtml(we);
        }
        return text;

    }

    public String getPageSource() {

        String html = null;
        int i = 0;
        boolean success;
        do
        {
            try
            {
                html = driver.getPageSource();
                success = true;
            }
            catch (Exception x){success = false;}
            if (!success)
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {}
            }

            i++;
        }
        while (!success && i < 5);

        return html;

    }

    public String getPageTitle()
    {
        return driver.getTitle();
    }

    public String getPageUrl()
    {
        return driver.getCurrentUrl();
    }

    public void maximize()
    {
        driver.manage().window().maximize();
    }

    public void navigateBack()
    {
        driver.navigate().back();
    }

    public void navigateForward()
    {
        driver.navigate().forward();
    }

    public void navigateRefresh()
    {
        driver.navigate().refresh();
    }

    public void navigateToUrl(String url)
    {
        driver.navigate().to(url);
    }

    public void changeToRemoteWebDriver(WebDriver d)
    {
        driver = (EventFiringWebDriver) d;
    }

    public void switchToActiveElement()
    {
        driver.switchTo().activeElement();
    }

    public void switchToWindow(String locatorName) {

        Set<String> handles = driver.getWindowHandles();
        for (String h : handles) {
            driver.switchTo().window(h);
            if (isElementPresent(locatorName))
                break;
        }

    }

    public void switchToNewTab() {
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        driver.getCurrentUrl();
    }

    public void switchToWindowHandle()
    {
        driver.switchTo().window(driver.getWindowHandle());
    }

    public void switchToTopWindow()
    {
        driver.switchTo().defaultContent();
    }

    public void switchFrames(String frame)
    {
        driver.switchTo().frame(frame);
    }

    public void switchAndCloseBrowser(){

        String beforeWinHandle = driver.getWindowHandle();
        String nextWinHandle="";
        try {
            for (String winHandle : driver.getWindowHandles()) {
                nextWinHandle=winHandle;
                driver.switchTo().window(winHandle);
            }
            if (!nextWinHandle.equals(beforeWinHandle)) {
                driver.close();
            }
            driver.switchTo().window(beforeWinHandle);
        }catch(Exception e){}

    }

    public void switchFrames(WebElement frame)
    {
        driver.switchTo().frame(frame);
    }

    public void switchFrames(int frame)
    {
        driver.switchTo().frame(frame);
    }

    public void switchWindow(String h)
    {
        driver.switchTo().window(h);
    }

    public void switchToAlert()
    {
        driver.switchTo().alert();
    }

    public void SwitchToTopWindowByHandle()
    {
        switchFrames(driver.getWindowHandle());
    }

    public String hoverText(String locatorName)  {
        String retVal="";
        if (waitForElementPresent(locatorName, 15)) {
            WebElement we = this.getWebElementWithLocatorKey(locatorName);
            new Actions(driver).moveToElement(we).perform();
            retVal = we.getText();
        }
        return  retVal;
    }

    public String Hover(String locatorName) throws Exception {

        String hoverresults = hoverText(locatorName);
        return hoverresults;

    }

    public void setToExpectedOptionAndClick(String locatorName, String replacementValue) {

        setToExpectedOption(locatorName, replacementValue, false);
        clickWhenClickable(locatorName,0);
        setToExpectedOption(locatorName, replacementValue,true);

    }

    public void setToExpectedOptionAndscrollIntoView(String locatorName, String replacementValue) {
        this.setToExpectedOption(locatorName, replacementValue, false);
        this.scrollIntoViewJS(locatorName);
        this.setToExpectedOption(locatorName, replacementValue, true);
    }
    public void setToExpectedOptionAndSendKeys(String locatorName, String replacementValue,String keys ) {
        setToExpectedOption(locatorName, replacementValue, false);
        sendKeys(locatorName,keys);
        setToExpectedOption(locatorName, replacementValue,true);
    }

    public void highlightText(String locator, int index){
        WebElement we = getWebElementWithLocatorKey(locator);
        String classname = we.getAttribute("name");
        if (classname.isEmpty()){
            classname = we.getAttribute("class");
            String focus = String.format("document.getElementsByClassName('%s')[%s].focus();", classname, index);
            String select = String.format("document.getElementsByClassName('%s')[%s].select();", classname, index);
            JavascriptExecutor js = driver;
            js.executeScript(focus);
            js.executeScript(select);
        }
        else {
            String focus = String.format("document.getElementsByName('%s')[%s].focus();", classname, index);
            String select = String.format("document.getElementsByName('%s')[%s].select();", classname, index);
            JavascriptExecutor js = driver;
            js.executeScript(focus);
            js.executeScript(select);
        }


    }

    public boolean setToExpectedOptionAndWaitUntilVisible(String locatorName, String replacementValue) {
        setToExpectedOption(locatorName, replacementValue, false);
        return waitUntilVisible(locatorName);
//        setToExpectedOption(locatorName, replacementValue,true);

    }

    public void HoverAndClick(String mainLocatorName, String subLocatorName) throws Exception {

        Actions builder = new Actions(driver);
        builder.moveToElement(getWebElement(mainLocatorName)).build().perform();
        builder.click();
        waitForElementPresent(subLocatorName, 5);
        this.getWebElementWithLocatorKey(subLocatorName).click();

    }

    public void CloseWindow()
    {
        driver.close();
    }

    public List<String> windowTitles() {

        Set<String> handles = driver.getWindowHandles();
        String currentHandle = driver.getWindowHandle();
        List<String> titles = null ;

        for (String handle : handles)
        {
            driver.switchTo().window(handle);
            titles.add(driver.getTitle());
        }

        driver.switchTo().window(currentHandle);

        return titles;

    }

    public boolean findAndSetWindowTitle(String title) {

        Set<String> handles = driver.getWindowHandles();
        boolean foundWin = false;
        String winTitle = "";
        for (String handle  : handles)
        {
            driver.switchTo().window(handle);
            winTitle = driver.getTitle();
            System.out.println("The title is: " + winTitle);
            if (winTitle.contains(title))
            {
                foundWin = true;
                driver.switchTo().window(handle);
                break;
            }
        }

        return foundWin;

    }

    public List<String> windowUrls() {

        Set<String> handles = driver.getWindowHandles();
        String currentHandle = driver.getWindowHandle();
        List<String> urls =null;
        for (String handle : handles)
        {
            driver.switchTo().window(handle);
            urls.add(driver.getCurrentUrl());
        }

        driver.switchTo().window(currentHandle);

        return urls;

    }

    public WebElement getWebElement(String locatorName) {

        try
        {
            this.element = this.getWebElementWithLocatorKey(locatorName);
            return this.element;
        }
        catch (Exception x)
        {
            System.out.println("No webelement found with " + locatorName);
        }
        return null;

    }

    public String closeAlertAndGetItsText(int iTimeout) {

        String alertText = "";
        Alert alert = null;
        int i = 0;
        while (ACCEPT_NEXT_ALERT)
        {
            try
            {
                i++;
                if (i == iTimeout)
                {
                    break;
                }
                Thread.sleep(1000);
                alert = driver.switchTo().alert();
                alertText = alert.getText();
                alert.accept();
                ACCEPT_NEXT_ALERT = false;
                break;
            }
            catch (Exception x)
            {
                ACCEPT_NEXT_ALERT = true;
            }
        }
        return alertText;

    }

    public void ntlmAuthentication(String username, String password) {

        String alertText = waitForAlertPresent(20);
        Alert alert = driver.switchTo().alert();
        alert.authenticateUsing(new UserAndPassword(username, password));

    }

    public String waitForAlertPresent(int iTimeout) {

        String alertText = null;
        Alert alert = null;
        int i = 0;
        while (ACCEPT_NEXT_ALERT)
        {
            try
            {
                i++;
                if (i == iTimeout)
                {
                    break;
                }
                Thread.sleep(1000);
                alert = driver.switchTo().alert();
                alertText = alert.getText();
                ACCEPT_NEXT_ALERT = false;
                break;
            }
            catch (Exception x)
            {
                ACCEPT_NEXT_ALERT = true;
            }
        }
        return alertText;
    }

    public String getAlertText() {

        String alertText = "";
        Alert alert = null;
        if (alertPresent())
        {
            alert = driver.switchTo().alert();
            alertText = alert.getText();
        }
        return alertText;

    }

    public void dragAndDrop(WebElement toDrag, WebElement toDropAfterElement) {

        Actions builder = new Actions(driver);

        Action dragAndDrop = builder.clickAndHold(toDrag)
                .moveToElement(toDropAfterElement)
                .release(toDropAfterElement)
                .build();

        dragAndDrop.perform();

    }

    public void dragAndDrop(String locatorNameOfElementToDrag, String locatorNameOfElementToDragTo) throws Exception {

        dragAndDrop(getWebElement(locatorNameOfElementToDrag), getWebElement(locatorNameOfElementToDragTo));
    }

    public void scrollExtremeRight(WebElement toHold) {

        Actions builder = new Actions(driver);

        Action scrollExtremeRight = builder.clickAndHold(toHold)
                .moveByOffset(-100,0)
                .build();
        try
        {
            scrollExtremeRight.perform();
        }
        catch (Exception x){ }

    }

    public void scrollExtremeLeft(WebElement toHold) {

        Actions builder = new Actions(driver);

        Action scrollExtremeRight = builder.clickAndHold(toHold)
                .moveByOffset(0, 40)
                .build();
        try
        {
            scrollExtremeRight.perform();
        }
        catch (Exception x) {}

    }

    public void SetAppFocus() throws Exception {

        clickAt("appWrapper",400,400);

    }

    public void clickAndMove(WebElement we, int xPos, int yPos) {

        Actions builder = new Actions(driver);

        try
        {
            builder.clickAndHold(we).moveByOffset(xPos, yPos).release().perform();
        }
        catch (Exception e) { System.out.println("clickAndHold error: " + e.getMessage()); }

    }

    public void scrollDownIntoView(String locatorName) throws Exception {

        RemoteWebElement we = (RemoteWebElement)this.getWebElementWithLocatorKey(locatorName);
        scrollDownIntoView(we);

    }

    public void scrollDownWithKeys(String locatorName){

        this.focus(locatorName);
        Actions action = new Actions(driver);
        action.sendKeys(Keys.CONTROL).sendKeys(Keys.END).perform();

    }

    public void scrollDownIntoView(RemoteWebElement we) {

        ((RemoteWebElement)we).click();
        Actions action = new Actions(driver);
        action.sendKeys(Keys.CONTROL)
                .sendKeys(Keys.DOWN).build().perform();

    }

    public void SendKeysEnhancedEncodedInBase64(String locatorName, String keys) {

        keys = decodeBase64(keys);
        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        sendKeysEnhanced(we, keys);

    }

    public void sendKeysEnhanced(String locatorName, String keys) {

        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        sendKeysEnhanced(we, keys);

    }

    public void sendKeys(WebElement we, String keys)
    {
        we.sendKeys(keys);
    }

    public void sendKeysEncodedInBase64(String locatorName, String keys) {

        keys = decodeBase64(keys);
        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        sendKeys(we, keys);

    }

    public void sendKeys(String locatorName, String keys) {

        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        sendKeys(we, keys);

    }

    public void clearAndSendKeys(String locatorName, String keys) {

        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        try {
            we.clear();
        }catch (Exception e){}
        sendKeys(we, keys);

    }

    public void sendKeysEnhanced(WebElement we, String keys) {

        Actions action = new Actions(driver);
        we.clear();
        action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).perform();
        action.sendKeys(Keys.DELETE)
                .sendKeys(Keys.BACK_SPACE)
                .sendKeys(Keys.HOME)
                .sendKeys(keys).build().perform();

    }

    public void sendKeysNumEnhanced(String keys) {

        Actions action = new Actions(driver);
        action.sendKeys(Keys.DELETE)
                .sendKeys(Keys.BACK_SPACE)
                .sendKeys(Keys.HOME)
                .sendKeys(keys).build().perform();

    }

    public void scrollVertical(boolean up) {

        Actions action = new Actions(driver);
        if (up) {
            action.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).keyUp(Keys.CONTROL)
                    .build().perform();
        }
        else {
                action.keyDown(Keys.CONTROL).sendKeys(Keys.END).keyUp(Keys.CONTROL)
                        .build().perform();
        }

    }

    public void sendUpDownArrow(boolean up,int numTimes) {

        for (int i = 0; i < numTimes; i++) {
            if (up) {
                robot.keyPress(KeyEvent.VK_WINDOWS);
                robot.delay(100);
                robot.keyPress(KeyEvent.VK_KP_UP);
                robot.delay(100);
                robot.keyRelease(KeyEvent.VK_KP_UP);
                robot.delay(100);
                robot.keyRelease(KeyEvent.VK_WINDOWS);
            }
            else{
                robot.keyPress(KeyEvent.VK_WINDOWS);
                robot.delay(100);
                robot.keyPress(KeyEvent.VK_PAGE_DOWN);
                robot.delay(100);
                robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
                robot.delay(100);
                robot.keyRelease(KeyEvent.VK_WINDOWS);
            }
        }

    }

    public void sendPageDown(){
        Actions action = new Actions(driver);
        action.sendKeys(Keys.PAGE_DOWN).build().perform();
    }

    public void sendTabKey(){
        Actions action = new Actions(driver);
        action.sendKeys(Keys.TAB).build().perform();
    }

    public void sendEnterKey(){
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ENTER).build().perform();
    }


    public void sendAnyKey(String key){
        Actions action = new Actions(driver);
        action.sendKeys(key).build().perform();
    }

    public void scrollDownJS(String pixels)  {
       driver.executeScript("scroll(0,"+pixels+")");
    }

    public boolean scrollIntoViewJS(String locatorName)  {
        WebElement we = getWebElementWithLocatorKey(locatorName);
        boolean retVal=false;
        try {
            driver.executeScript("arguments[0].scrollIntoView(true);", we);
            retVal=true;
        }catch(Exception e){}
        return retVal;
    }

    public boolean scrollIntoView(String locatorName)  {
        boolean retVal=false;
        try
        {
            scrollIntoView(findElementOnPage(locatorName));
            retVal=true;
        }
        catch (Exception e)
        {
            // e.printStackTrace();
        }
        return retVal;
    }

    public void scrollIntoView(WebElement we) {

        try
        {
            if (CURRENT_BROWSER.equals("internet explorer"))
            {
                Point scroll = ((RemoteWebElement)we).getLocation();
                ((RemoteWebElement)we).click();
            }
            else
            {
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",we);
            }
        }
        catch (Exception x) {
            if (CURRENT_BROWSER.equals("internet explorer"))
            {
                System.out.println("IE8 Error: " + x.getMessage());
            }
            else
            {
                System.out.println("Error: " + x.getMessage());
            }
            robot.keyPress(KeyEvent.VK_END);
        }

    }

    public void clickElementByLocation(String locatorName) throws Exception {

        WebElement we = getWebElementWithLocatorKey(locatorName);
        Point coords = we.getLocation();
        int x=coords.getX();
        int y=coords.getY();
        clickAt(locatorName,x,y);
    }

    public WebElement findElementOnPage(String locatorName) throws Exception {

        WebElement we = getWebElementWithLocatorKey(locatorName);
        RemoteWebElement element = (RemoteWebElement)we;
        Point hack = element.getLocation();
        return element;

    }

    public boolean waitWhilePageTitleExists(String title, int iTimeout) throws InterruptedException {

        boolean retVal = false;
        int i = 0;
        while (getPageTitle().contains(title))
        {
            Thread.sleep(1000);
            i++;
            if (i > iTimeout)
            {
                retVal = true;
                break;
            }
        }
        return retVal;

    }

    public String textJS(String locatorName) throws Exception {

        WebElement element = this.getWebElementWithLocatorKey(locatorName);
        if (CURRENT_BROWSER.equals("internet explorer"))
        {

            return "";
        }
        else
        {
            return (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerText",element);
        }

    }

    public void clickMenuSelectionOnMouseOver(String locatorNameMouseOver, String locatorNameClick) {

        clickMenuSelectionOnMouseOver(this.getWebElementWithLocatorKey(locatorNameMouseOver), this.getWebElementWithLocatorKey(locatorNameClick));

    }

    public void clickMenuSelectionOnMouseOver(WebElement MouseOver, WebElement click) {

        Actions action = new Actions(driver);
        action.moveToElement(MouseOver).moveToElement(click).click().build().perform();

    }

    public void printText(String xpath) {

        List<WebElement> weList = driver.findElements(By.xpath(xpath));
        for (WebElement we : weList)
        {
            try
            {
                if (we.getText() != "")
                {
                    System.out.println("getText():" + we.getText());
                }
            }
            catch (Exception e)
            {
                System.out.println("Exception: " + e.getMessage());
            }
        }

    }

    public void switchToLastModalContent(String locatorName, int itimeout) {

        try
        {
            WebDriverWait wait = new WebDriverWait(driver, itimeout);
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(locatorName));
            driver.switchTo().frame(this.getWebElementWithLocatorKey(locatorName));
        }
        catch (Exception x) { }

    }

    public String getTextAsList(String locatorName) {

        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        String actList = we.getText().replace("\r\n", "|").split("|").toString();
        return actList;

    }

    public String getAttribAsList(String locatorName,String attribute) throws Exception {

        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        String actList = we.getText().replace("\r\n", "|").split("|").toString();
        return actList;

    }

    public void waitForPageTitleToContain(String title, int timeout) {

        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.titleContains(title));

    }

    public void waitForPageTitleNotToContain(String title, int timeout) {

        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.not(ExpectedConditions.titleContains(title)));

    }

    public void waitForTextToContain(String locatorName, String text, int timeout) {

        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.textToBePresentInElementLocated(this.getByWithLocatorKey(locatorName), text));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void rightClick(String locatorName) throws Exception {

        rightClick(this.getWebElementWithLocatorKey(locatorName));

    }

    public void rightClick(WebElement we)
    {
        new Actions(driver).contextClick(we).perform();
    }

    public String getElementType(WebElement element) {

        String _type;
        String _style;
        System.out.println(element.getText());
        WebElement _inputElement = driver.findElement(By.id(element.getAttribute("for")));
        boolean IsMultiple = attributeExists(driver.findElement(By.id(element.getAttribute("for"))), "multiple");
        _type = _inputElement.getTagName();
        switch (_type)
        {
            case "table":
                return "Date Field";
            case "select":
                if (IsMultiple)
                    return "List Box";
                else
                    return "Dropdown List";
        }
        _type = _inputElement.getAttribute("type");
        switch (_type)
        {
            case "checkbox":
                return _type;
            case "text":
                _style = _inputElement.getAttribute("style");
                if (_style.contains("text-align: right"))
                    return "text:Number";
                else return _type;
            default: return _type;
        }

    }

    public void clickJS(String locatorName) throws Exception {

        WebElement we = this.getWebElementWithLocatorKey(locatorName);
        clickJS(we);

    }

    public void clickJS(WebElement we)
    {
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",we);
    }

    public boolean verifyText(String locatorName, String text) {
        return this.getWebElementWithLocatorKey(locatorName).getText().equals(text);
    }

    public void sendKeysList(Multimap<String, String> elements){
        Multimap<String, String>  updateElements= elements;
        Iterator<String> it = updateElements.keySet().iterator();
        String key;
        Collection<String> value;
        while (it.hasNext()){
            key =(String)it.next();
            value= updateElements.get(key);
            clearAndSendKeys(key,value.toArray()[0].toString());
        }
    }

    public void clickList(Multimap<String, String> elements){
        Multimap<String, String>  updateElements= elements;
        Iterator<String> it = updateElements.keySet().iterator();
        String key;
        Collection<String> values;
        while (it.hasNext()){
            key =(String)it.next();
            values= updateElements.get(key);
            for (String value : values){
                setToExpectedOptionAndClick(key,value);
            }
        }
    }

    //Handles form elements based on the js form element attributes, takes a map (must be unique values)
    public void formElementHandler(String formElement, String value,List<String> selectValues) {

        Map<String, String> expectedProfile = new LinkedHashMap<String, String>();
        expectedProfile.put(formElement,value);
        formElementHandler(expectedProfile,selectValues);

    }

    public void formElementHandler(Map<String,String> userValues,List<String> selectValues){

        boolean clickControl;
        boolean isChecked;

        for(Map.Entry<String, String> userValue : userValues.entrySet()) {

            setToExpectedOption("app.locIdentifier", userValue.getKey(), false);
            waitForElementPresent("app.locIdentifier", 5);

            if(IsMultiple("app.locIdentifier")){

                selectMultipleOptions("app.locIdentifier", selectValues);

            }
            else if(IsLink( "app.locIdentifier" ))

                click( "app.locIdentifier" );

            else if(IsButton( "app.locIdentifier" ))

                waitAndClick( "app.locIdentifier" );

            else if(IsCollapsible( "app.locIdentifier" ))

                waitAndClick( "app.locIdentifier" );

            else if(IsCheckBox( "app.locIdentifier" )) {

                try {

                    clickControl = userValue.getValue().equalsIgnoreCase("Select");
                    isChecked = isChecked("app.locIdentifier");
                    if ( ( clickControl != isChecked ) )
                        click("app.locIdentifier");
                }
                catch (Exception e){}
            }
            else if (IsRadio( "app.locIdentifier" ))

                try {

                    clickControl=userValue.getValue().equalsIgnoreCase("Select");
                    isChecked= isChecked("app.locIdentifier");
                    if ((clickControl!=isChecked))
                        click("app.locIdentifier");

                }
                catch (Exception e){}

            else if (IsSelect("app.locIdentifier")) {

                String value = userValue.getValue();
                selectText("app.locIdentifier", value);

            }
            else if (IsText("app.locIdentifier")||(IsEmail("app.locIdentifier"))||(IsDate("app.locIdentifier"))) {

                clear("app.locIdentifier");
                sendKeys("app.locIdentifier", userValue.getValue());

            }
            else{

                click("app.locIdentifier");

            }

            setToExpectedOption("app.locIdentifier", userValue.getKey(), true);

        }
    }

    public boolean IsLink( String locatorName ){

        boolean retVal = false;
        String data;

        try {
            data = getAttribute(locatorName, "href");
            retVal = data != null;
        }
        catch ( Exception e ){}
        return retVal;

    }

    public boolean IsMultiple(String locatorName){

        boolean retVal=false;
        String data;

        try {
            data=getAttribute(locatorName, "multiple");
            retVal = data.equals("true");
        }
        catch (Exception e){}
        return retVal;

    }

    public boolean IsDate(String locatorName){

        boolean retVal = false;
        String  controlType = getAttribute(locatorName, "type");

        if (controlType == null)
            return false;
        return controlType.equals("date");

    }

    public boolean IsText(String locatorName){

        boolean retVal = false;
        String  controlType = getAttribute(locatorName, "type");

        if (controlType == null)
            return false;
        return controlType.equals("text");

    }

    public boolean IsEmail(String locatorName){

        boolean retVal = false;
        String  controlType = getAttribute(locatorName, "type");

        if (controlType == null)
            return false;

        return controlType.equals("email");

    }

    public boolean IsRadio(String locatorName){

        boolean retVal = false;
        String  controlType = getAttribute(locatorName, "type");

        if (controlType==null)
            return false;

        return controlType.equals("radio");

    }

    public boolean IsCheckBox(String locatorName){

        boolean retVal = false;
        String  controlType = getAttribute(locatorName, "type");

        if (controlType == null)
            return false;

        return controlType.equals("checkbox");
    }

    public boolean IsButton(String locatorName){

        boolean retVal = false;
        String  controlType = getAttribute(locatorName, "type");

        if (controlType == null)
            return false;

        return controlType.equals("submit");

    }

    public boolean IsSelect(String locatorName){

        String xpath =getLocatorValue(locatorName);
        String fullXpath=xpath.replace("//*","//*/select");

        boolean retVal = false;
        try {
            retVal=getWebElementByXpath(fullXpath).isDisplayed();
        } catch (Exception e) {
        }
        return retVal;

    }

    public boolean IsRequired(String locatorName){

        boolean retVal = false;
        String data;

        try {
            String xpath = getLocatorValue(locatorName);
            data = getAttribute(locatorName, "class");
            data=getAttribute(locatorName, "class");
            retVal = data.contains("ng-valid-required");
        }
        catch (Exception e){}
        return retVal;
    }

    public boolean IsCollapsible(String locatorName){

        String controlType = "";
        boolean retVal;

        try {
            controlType = getAttribute(locatorName, "class");
        }
        catch (Exception e){
            retVal= false;}
        if (controlType == null) {
            retVal= false;
        }
        retVal=controlType.contains("glyphicon");
        return retVal;
    }

    public boolean IsCollapsibleClosed(String locatorName){

        String controlType = "";
        boolean retVal;
        try {
            controlType = getAttribute(locatorName, "class");
        }
        catch (Exception e){
            retVal= false;}
        if (controlType == null) {
            retVal= false;
        }
        retVal=controlType.contains("glyphicon-plus-sign");
        return retVal;

    }

    public List findAllLinks()
    {

        List<WebElement> elementList = new ArrayList();

        elementList = driver.findElements(By.tagName("a"));

        elementList.addAll(driver.findElements(By.tagName("img")));

        List finalList = new ArrayList();

        for (WebElement we : elementList)

        {

            if (we.getAttribute("href") != null)

            {

                finalList.add(we);

            }

        }

        return finalList;
    }

    public String isLinkBroken(URL url) throws Exception
    {

        //url = new URL("http://yahoo.com");

        String response = "";

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try

        {

            connection.connect();

            response = connection.getResponseMessage();

            connection.disconnect();

            return response;

        }

        catch(Exception exp)

        {

            return exp.getMessage();

        }

    }

    public String sendPost(String url, String authorization,String textFile) throws Exception
    {
        URL postURL = new URL(url);
        postURL = new URL("http://localhost:80/test1.txt");
        File file = new File("E:/test1.txt");
        HttpURLConnection connection = (HttpURLConnection) postURL.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestProperty("Content-Type", "text/plain");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Cache-Control", "no-cache");
        connection.setRequestProperty("Authorization", authorization);

        try  {
            BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(textFile));
            int i;
            // read byte by byte until end of stream
            while ((i = bis.read()) > 0) {
                bos.write(i);
            }
         System.out.println(((HttpURLConnection) connection).getResponseMessage());
    }
    catch (IOException e) {
    }
     try
        {
            connection.connect();
            String response = connection.getResponseMessage();
            connection.disconnect();
            return response;
        }

        catch(Exception exp)

        {

          return exp.getMessage();

        }

    }

    public void validateLinkPrintResults(String url) {
        if (!url.isEmpty())
            getUrl(url);
        List<WebElement> urls = findAllLinks();
        for (WebElement we : urls) {

            try

            {

                System.out.println("URL: " + we.getAttribute("href") + " returned " + isLinkBroken(new URL(we.getAttribute("href"))));

                //System.out.println("URL: " + element.getAttribute("outerhtml")+ " returned " + isLinkBroken(new URL(element.getAttribute("href"))));

            } catch (Exception exp)

            {

                System.out.println("At " + we.getAttribute("innerHTML") + " Exception occured -&gt; " + exp.getMessage());

            }

        }

    }

    public String setXPathFromLocator(String locator){

        String locatorXpath = getLocatorValue(locator);
        autProps.setProperty("app.locIdentifier", locatorXpath);
        return locatorXpath;

    }

    public boolean  compareLists(List<String> expectedResults, List<String> actualResults, boolean sort, boolean contains,boolean replaceLineBreaks,String createListName, boolean debug) {
        List<String> newActualResults=new ArrayList<String>();
        List<String> newExpectedResults=new ArrayList<String>();
       if (replaceLineBreaks){
           for (String actualResult : actualResults) {
               newActualResults.add(actualResult.replaceAll("\\r?\\n","-"));
           }
           for (String expectedResult : expectedResults) {
               newExpectedResults.add(expectedResult.replaceAll("\\r?\\n","-"));
           }
       }
     return compareLists(newExpectedResults, newActualResults, sort, contains,createListName, debug);

}

    public boolean  compareLists(List<String> expectedResults, List<String> actualResults, boolean sort, boolean contains,String createListName, boolean debug) {
        boolean retVal = true;
        String expValue="";
        String actValue="";
        boolean matchResult=false;
        // Do not sort when using contains, the order will be different for each
        if (sort) {
            Collections.sort(expectedResults);
            Collections.sort(actualResults);
         }
         if (!debug) {
            if (actualResults.size() != expectedResults.size()) {
                try {
                    System.out.println("\n***** Size Mismatch: Actual is " + actualResults.size() + " Expected is " + expectedResults.size() + " *****");
                    retVal=false;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    retVal = false;
                }
            }
        }
        try {
            if (debug){
                System.out.println("\nprivate List<String> get"+createListName+"() {");
                System.out.println("List<String> "+createListName+"= new ArrayList<String>();");
            }

            for (int i = 0; i < actualResults.size(); i++) {
                actValue = actualResults.get(i).toString().trim().replaceAll("\\r?\\n","-");
                expValue= expectedResults.get(i).toString().trim().replaceAll("\\r?\\n","-");;
                if (contains)
                    matchResult=actValue.contains(expValue);
                else
                    matchResult=actValue.equals(expValue );
                if (!matchResult) {
                    if (debug) {
                         System.out.println(createListName+".add(\"" + actValue.trim() + "\");");
                    }
                    else{
                        System.out.println("\nNo Match: \nActual Value:    " + actValue.trim() + "\nExpected Value:  " + expValue.trim());
                        retVal = false;
                    }
                } else {
                    if (debug) {
                         System.out.println(createListName+".add(\""+ actValue            + "\");");
                    }
                }
            }
            if (debug)
                System.out.println("\nreturn "+createListName+";\n}");

        }
        catch(Exception e){
            System.out.println(e.getMessage());
            retVal=false;
        }
        return retVal;
    }

    public String getTextRetry(String locatorName, Integer attempts){
        String value="";
        try {
            for (int i = 0; i < attempts; i++) {

                value=getText(locatorName);
                if (!value.isEmpty())
                    break;
            }
        }catch (Exception e){}
        return value;
    }

    public void sendKeys(String keyCommand,int numTimes){
        Actions actions = new Actions(driver);
        for (int i=0;i<numTimes;i++) {
            actions.sendKeys(keyCommand).perform();

        }
    }

    public List<String> getIdByList(String locatorName, int listcount) {
        int i = 0;
        waitForElementPresent(locatorName, 5);
        List<WebElement> weList = getWebElements(locatorName);
        System.out.println(weList);
        String id = "";
        List<String> rList = new ArrayList<String>();
        for (WebElement we : weList) {
            id = we.getAttribute("id");
            rList.add(id);
            if (listcount != 0) {
                i++;
                if (i == listcount)
                    break;
            }

        }
        return rList;
    }

    public List<String> setExpectedOptionAndGetTextByList(String locatorName,String option, int listcount){
        List <String> webElementTextList = new ArrayList<String>();
        setToExpectedOption(locatorName,option,false);
        webElementTextList=getTextByList(locatorName,listcount);
        setToExpectedOption(locatorName,option,true);
        return webElementTextList;
    }

    public List<String> getTextByList(String locatorName, boolean uppercase, int listcount){
        List <String> weTextList = new ArrayList<String>();
        List <String> weTextListUpperCase = new ArrayList<String>();
        weTextList=getTextByList(locatorName,listcount);
        for (String item : weTextList) {
            weTextListUpperCase.add(item.toUpperCase());
        }
        return weTextListUpperCase;

    }

   public List<String> getTextByList(String locatorName, int listcount) {

        int i = 0;
        waitForElementPresent(locatorName,5);
        List<WebElement> weList = getWebElements(locatorName);
        String text = "";
        List<String> rList = new ArrayList<String>();
        for (WebElement we : weList) {
            text = we.getText();
            try {
                if (text.isEmpty()) {
                    text = this.getAttribute(locatorName, "value");
                }
                if (text == null) {
                    text = getInnerHtml(we);
                }
            }catch(Exception e){text="";}
            rList.add(text);
            if (listcount !=0) {
                i++;
                if (i==listcount)
                    break;
            }
        }
        return rList;

    }

    public String getAttributeIfTextFound(String locatorName,String attribute,String text) {

        int i = 0;
        List<WebElement> weList = getWebElements(locatorName);
        String returnAttribValue= "";
        List<String> rList = new ArrayList<String>();
        for (WebElement we : weList) {
            if(we.getText().contains(text)) {
                returnAttribValue = we.getAttribute(attribute);
                break;
            }
        }
        return returnAttribValue;

    }

    public void waitUntilAttribValueChanges(String locatorName, String attributeName, String expAttribValue, Integer iTimout) {

        int i = 0;
        String curAttibValue="";
        try {
            for (i = 0; i < iTimout; i++) {
                Thread.sleep(1000);
                curAttibValue = getAttribute(locatorName, attributeName);
                //               System.out.println("Locator is: "+locatorName+" Attrib Name is "+attributeName+" Attrib Value is: "+curAttibValue);
                if (curAttibValue.equals(expAttribValue))
                    break;
            }
        }
        catch ( Exception e ){e.printStackTrace();}

    }

    public void waitWhileAttribExists(String locatorName, String attributeName,Integer iTimout) {

        int i = 0;
        String curAttibValue="";
        try {
            for (i = 0; i < iTimout; i++) {
                Thread.sleep(1000);
                curAttibValue = getAttribute(locatorName, attributeName);
                //               System.out.println("Locator is: "+locatorName+" Attrib Name is "+attributeName+" Attrib Value is: "+curAttibValue);
                if (curAttibValue==null)
                    break;
            }
        }
        catch ( Exception e ){e.printStackTrace();}

    }

    public void switchToNextBrowserTab() {

        //TODO

    }

    public String postFileUpload(String fileName) throws IOException {
        InputStream instream=new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        File file = new File(fileName);
        FileEntity entity = new FileEntity(file,
                ContentType.create("text/plain", "UTF-8"));

        HttpPost httppost = new HttpPost("https://www.googleapis.com/upload/drive/v3?uploadType=file");
        httppost.setEntity(entity);
        if (entity != null) {
            instream = entity.getContent();
            try {
                System.out.println(instream.toString());
            } finally {
                instream.close();
            }
        }
//        POST https://www.googleapis.com/upload/drive/v3?uploadType=media HTTP/1.1
//        Content-Type: image/jpeg
//        Content-Length: [NUMBER_OF_BYTES_IN_FILE]
//        Authorization: Bearer [YOUR_AUTH_TOKEN]
//
//                [JPEG_DATA]
        return instream.toString();
    }
     //Take Screenshot with AShot
    public Screenshot takeAShotScreenshot (String locatorName, String secondLocator) {
        WebElement element=getWebElement(locatorName);
        List<WebElement> elements=new ArrayList<>();
        elements.add(element);
        if (secondLocator!=null) {
            WebElement element2 = getWebElement(locatorName);
            elements.add(element2);
        }
        //Take screenshot with Ashot
        //see https://github.com/yandex-qatools/ashot for options
        Screenshot elementScreenShot  = new AShot()
                .imageCropper(new IndentCropper()
                .addIndentFilter(blur()))
                .takeScreenshot(driver, elements);
        //if coordinates are not always right use another coords provider.
         /*Screenshot elementScreenShot = new AShot()
                .coordsProvider(new WebDriverCoordsProvider())
                .takeAShotScreenshot(driver,element);*/
        //Print element size
 //       String size = "Height: " + elementScreenShot.getImage().getHeight() + "\n" +
 //               "Width: " + elementScreenShot.getImage().getWidth() + "\n";
 //       System.out.print("Size: " + size);

        return elementScreenShot;
    }

    public void getAllAttributes(String locatorName,String attrib){
        WebElement element = getWebElement(locatorName);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        Object aa=executor.executeScript("var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;", element);
        System.out.println(aa.toString());
    }

    public void pasteClipboard(String text) {
        StringSelection selection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
    }



  }
