package com.taite.Util;

import com.google.common.io.Files;
import org.im4java.core.CompareCmd;
import org.im4java.core.IMOperation;
import org.im4java.process.ProcessStarter;
import org.im4java.process.StandardStream;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * User: pboatwright
 * Date: 4/11/13
 * Time: 2:06 PM
 */
public class ScreenshotUtil {

    //Test Screenshot directory
    public String testScreenShotDirectory;
    //Main Directory of the test code
    public String currentDir = System.getProperty("user.dir");

    //Main screenshot directory
    public String parentScreenShotsLocation = currentDir + "\\ScreenShots\\";

    //Element screenshot paths
    public String baselineImageFilePath;
    public String actualImageFilePath;
    public String differenceImageFilePath;

    public String baselineScreenShotPath= parentScreenShotsLocation +"Baseline";
    public String actualScreenShotPath=parentScreenShotsLocation + "Actual";
    public String differenceScreenShotPath=parentScreenShotsLocation + "Diff";;
    public String differenceScreenShotParentPath=parentScreenShotsLocation + "Diff";;

    //Image files
    public File baselineImageFile;
    public File actualImageFile;
    public File differenceImageFile;
    public File differenceFileForParent;
    public File imageFile;
    public File differencesFolder;

    public void createScreenShotParentFolders() throws IOException {

        //Create screenshot and differences folders if they do not exist
        createFolder(baselineScreenShotPath);
        createFolder(actualScreenShotPath);
        createFolder(differenceScreenShotPath);

    }

    public void createScreenShotTestFolder(String className,String browser) throws IOException {
        //Create screenshot and differences folders if they do not exist
        String testClassName=className;
        baselineImageFilePath=baselineScreenShotPath+"\\"+testClassName+"\\"+browser;
        actualImageFilePath=actualScreenShotPath+"\\"+testClassName+"\\"+browser;
        differenceImageFilePath=differenceScreenShotPath+"\\"+testClassName+"\\"+browser;
        differenceScreenShotParentPath=differenceScreenShotPath+"\\"+testClassName+"\\"+browser+"\\CurrentDiffs";
        createFolder(baselineImageFilePath);
        createFolder(actualImageFilePath);
        createFolder(differenceImageFilePath);
        createFolder(differenceScreenShotParentPath);
     }
    //Create Folder Method
    public void createFolder (String path) {
        File testDirectory = new File(path);
        if (!testDirectory.exists()) {
            if (testDirectory.mkdirs()) {
                System.out.println("Directory: " + path + " is created!");
            } else {
                System.out.println("Failed to create directory: " + path);
            }
        }
    }

    //Write
    public boolean verifyScreenShot (Screenshot screenshot, String testName, boolean baseline) throws Exception {
        boolean retVal=true;
        String name=testName+".png";
        baselineImageFile = new File(baselineImageFilePath+"\\"+name);
        actualImageFile = new File(actualImageFilePath+"\\"+name);
        differenceImageFile = new File (differenceImageFilePath+"\\"+name);
        differencesFolder=new File(differenceScreenShotParentPath);
        //For copying difference to the parent Difference Folder
         differenceFileForParent = new File (differenceScreenShotParentPath+"\\"+name);
        if (differenceFileForParent.exists())
            differenceFileForParent.delete();
        if (actualImageFile.exists())
            actualImageFile.delete();

        if (baseline)
            imageFile=baselineImageFile;
        else
            imageFile=actualImageFile;
        try {
            ImageIO.write(screenshot.getImage(), "PNG", imageFile);
            imageFile.createNewFile();
            if (!baseline)
                retVal= doComparison(screenshot);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retVal;

    }

    //Compare Operation
    public boolean doComparison (Screenshot elementScreenShot) {
        boolean retVal=true;
        String baselineFilePath=baselineImageFile.getAbsoluteFile().toString();
        String actualFilePath=actualImageFile.getAbsoluteFile().toString();
        String diffFilePath=differenceImageFile.getAbsoluteFile().toString();
        try {
            ImageIO.write(elementScreenShot.getImage(), "PNG", actualImageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Did we capture baseline image before?
        if (baselineImageFile.exists()){
            //Try to use IM4Java for comparison
            try {
                compareImagesWithImageMagick(baselineFilePath, actualFilePath, diffFilePath);
                retVal=true;
            } catch (Exception e) {
                retVal=false;
                System.out.println (baselineFilePath.substring(baselineFilePath.lastIndexOf("\\")).replace("\\","").replace(".png","")+" failed to match the baseline!");
                System.out.println ("See "+ diffFilePath+ " for differences");
            }
        } else {
            System.out.println("No screenshot found for "+baselineFilePath.substring(baselineFilePath.lastIndexOf("\\")).replace("\\","").replace(".png","")+". Copying it into the screenshot folder.\n");
            //Put the screenshot to the specified folder
            try {
                ImageIO.write(elementScreenShot.getImage(), "PNG", baselineImageFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            retVal=false;
        }
        return retVal;
    }

    //ImageMagick Compare Method
    public void compareImagesWithImageMagick (String expected, String actual, String difference) throws Exception {
        //http://www.swtestacademy.com/visual-testing-imagemagick-selenium/
        // This class implements the processing of os-commands using a ProcessBuilder.
        // This is the core class of the im4java-library where all the magic takes place.
        ProcessStarter.setGlobalSearchPath("C:\\Program Files\\ImageMagick-7.0.5-Q16");
        // This instance wraps the compare command
        CompareCmd compare = new CompareCmd();

        // Set the ErrorConsumer for the stderr of the ProcessStarter.
        compare.setErrorConsumer(StandardStream.STDERR);

        // Create ImageMagick Operation Object
        IMOperation cmpOp = new IMOperation();

        //Add option -fuzz to the ImageMagick commandline
        //With Fuzz we can ignore small changes
        cmpOp.fuzz(10.0);

        //The special "-metric" setting of 'AE' (short for "Absolute Error" count), will report (to standard error),
        //a count of the actual number of pixels that were masked, at the current fuzz factor.
        cmpOp.metric("AE");

        // Add the expected image
        cmpOp.addImage(expected);

        // Add the actual image
        cmpOp.addImage(actual);

//        // This stores the difference
        cmpOp.addImage(difference);

        try {
            //Do the compare
            compare.run(cmpOp);
        }
        catch (Exception ex) {
            System.out.print(ex.toString().split(":")[0]+": \n\n");
            //Put the difference image to the global differences folder
            Files.copy(differenceImageFile,differenceFileForParent);
            File actualFile=new File(actual);
            throw ex;
        }
    }

    //aqat original
    public static String takeScreenshot(WebDriver driver) {

        String url = "Not Available";

        try {

            File screenShot = ( (TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//            String sourceDir = "screenshots/";
            String sourceDir = "screenshots/ErrorScreenShots/";
            String sourceAbsoluteFilePath = sourceDir + screenShot.getName();

            File srcdest = new File(sourceDir);
            if(!srcdest.exists()) {
                srcdest.mkdir();
            }
            srcdest.setWritable(true);
            srcdest.setReadable(true);

            File srcFile = new File(sourceAbsoluteFilePath);
            srcFile.setReadable(true);
            srcFile.setWritable(true);
            FileUtils.moveFile(screenShot, srcFile);

            if(srcFile.getAbsolutePath().toLowerCase().contains("jenkins") || srcFile.getAbsolutePath().toLowerCase().contains("jenkins-")) {
                // EXAMPLE :
                // WORKSPACE SERVER ROOT *nix (srcFile.getAbsolutePath): /var/jenkins-home/jobs/sureview_CallCenterApplication_Trunk_Deploy_Dev/workspace/qa/screenshots/screenshot6534837832969454301.png
                // WORKSPACE SERVER ROOT windows (srcFile.getAbsolutePath): e:\jenkins\workspace\sureview_Acceptance_Test\qa\screenshots\screenshot3398590140847418524.png
                // WORKSPACE URL: http://build.forcepoint.com/view/sureview/job/sureview_Acceptance_Test/ws/qa/screenshots/screenshot1615479711504443756.png
                String strSrcFileAbsolutePath = srcFile.getAbsolutePath();
                // System.out.println("srcFile.getAbsolutePath(): " + strSrcFileAbsolutePath);
                String[] serverPath;
                String jenkinsJob = "";
                if(strSrcFileAbsolutePath.startsWith("/")){ //*nix
                    serverPath = strSrcFileAbsolutePath.split("/");
                    jenkinsJob = serverPath[4];
                }
                else{ //windows
                    serverPath = strSrcFileAbsolutePath.split("\\\\"); //split on backslash
                    jenkinsJob = serverPath[3];
                }
                String jenkinsProject = jenkinsJob.split("_")[0];
                url = "http://build.forcepoint.com/view/"+ jenkinsProject + "/job/" + jenkinsJob + "/ws/qa/screenshots/" + srcFile.getName();
            }
            else {
                url = srcFile.getAbsolutePath();
            }

        } catch (IOException e) {
            System.out.print("Screenshot: Failed to capture...");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.print("Screenshot: Failed attempt...");
            e.printStackTrace();
        }

        System.out.println("Screenshot: " + url);
        return url;

    }




}
