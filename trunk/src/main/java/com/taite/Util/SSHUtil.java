package com.taite.Util;

import com.jcraft.jsch.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by pboatwright on 6/16/2015.
 */
public class SSHUtil{

    public String getInputStreamFromServer(String username, String password, String server, String commandType, String command, boolean removeLineBreaks) throws JSchException, IOException {

        byte[] tmp = new byte[1024];
        String result = "";
        Channel channel;
        InputStream inStream;
        OutputStream outStream;

        try {

            channel = connectToSession(username,password,server).openChannel(commandType);
            ((ChannelExec)channel).setCommand(command);
            channel.setInputStream(null);
            channel.setOutputStream(null);
            ((ChannelExec)channel).setErrStream(System.err);
            inStream=channel.getInputStream();
            outStream=channel.getOutputStream();

            //tty required
            ((ChannelExec)channel).setPty(true);
            channel.connect();

            //sudo requires re-input of password
            outStream.write(("sudo su".getBytes()));
            outStream.write((password + "\n").getBytes());
            outStream.flush();

        }
        catch (JSchException e) {

            throw new IOException("Could not open exec channel with command " + command,e);

        }
        while(true){

            while(inStream.available()>0) {

                int i = inStream.read(tmp, 0, 1024);
                if (i < 0) break;
                result = new String(tmp, 0, i);

            }

            if(channel.isClosed()){

                if(inStream.available()>0) continue;
                break;
            }
            try{
                Thread.sleep(1000);}catch(Exception ee){}
        }

        //cleanup
        if (removeLineBreaks)

            result= result.replace("[sudo] password for "+username+": ","").replaceAll("\\r\\n", "");

        return result;

    }


    public  Session connectToSession(String username,String password,String server){

        JSch jsch=new JSch();
        String host=null;
        Session session= null;

        try {

            session = jsch.getSession(username, server, 22);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
        } catch (Exception e) {
            e.printStackTrace();

        }

        return session;

    }

    /**
     1. Copy latest build from share (\\10.214.67.53\share\builds) to ""/mnt/crypt0/tmp/"
     2. cd /mnt/crypt0/tmp/
     3. tar xvfz 8.1.5.10133_Hotfix.tgz
     4. cd 8.1.5.xxxxx_Hotfix
     5. run ./install.sh
     6. License: (optional)
     1. Get the fit-dds file from \\10.214.68.53\share\QA\license
     2. Log in to the Command Center and navigate to Settings > Upload. Select the license file for the
     upload. The file will be saved to /mnt/crypt0/tmp/.
     3. Move the file to a location where the server will recognize it:
     mv /mnt/crypt0/tmp/fit-dds /mnt/crypt0/conf/fit-dds
     4. Run chmod 777 fit-dds to modify the permissions to the file.
     4. Log out of the Command Center and then log back in again
     5. Then input the license key: 6Q5T322B4CQG3Q3K
     7. Copy agent bundle from share (\\10.214.68.53\share\Application Versions\8.1\8.1.5\windows agent bundle_final\windows_agent_bundle-SV_10134.adp
     8. Enable all Collectors
     9. Create a group and add users
     10. Import Policies From from share: \\10.214.68.53\share\Application Versions\8.1\SV_Auxiliary_Disk\Policies_and_Reports\8.1-Release_Policy_Pack.zip
     11. Enable policies
     12. Add categories to group.
     */
 }




