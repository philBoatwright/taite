package com.taite.Configuration;

import com.taite.Listener.WebDriverEventAdapter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestContext;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


/**
 * User: pboatwright
 * Date: 4/22/13
 * Time: 9:19 AM
 */

@Augmentable
public class TaiteWebDriverConfig {

    /**
     * Provides a single instance of an augmented RemoteWebDriver for the given
     * Browser, Platform and Version. Augmentation is required for use of the Screenshot on failure within the TestListener
     * Supported browsers: "chrome", "internet explorer", "firefox"
     *
     * @param browser
     * @param platform
     * @param version
     * @return
     * @throws MalformedURLException
     */
     public WebDriver augmentedRemoteWebDriver(String browser, String version, String platform, ITestContext testContext,String gridHub) throws MalformedURLException, InterruptedException {

        RemoteWebDriver remoteWebDriver = getRemoteWebDriver(browser, version, platform, gridHub);
        Augmenter aug = new Augmenter();
        WebDriver driver = aug.augment(remoteWebDriver);
        testContext.setAttribute("webDriver", driver);
        return driver;

    }

    /**
     * Provides a single instance of an augmented WebDriver for the given
     * Browser, Platform and Version. Augmentation is required for use of the Screenshot on failure within the TestListener
     * Supported browsers: "chrome", "internet explorer", "firefox"
     *
     * @param browser
     * @param platform
     * @param version
     * @return
     * @throws MalformedURLException
     */
    public WebDriver augmentedWebDriver(String browser, String version, String platform, ITestContext testContext) throws MalformedURLException, InterruptedException {

        WebDriver webDriver = webDriver(browser, platform, version, testContext);
        Augmenter aug = new Augmenter();
        WebDriver driver = aug.augment(webDriver);
        testContext.setAttribute("webDriver", driver);
        return driver;

    }

    /**
     * Provides a single instance of a RemoteWebDriver for the given
     * Browser, Platform and Version
     * Supported browsers: "chrome", "internet explorer", "firefox"
     * @param browser
     * @param platform
     * @param version
     * @return
     * @throws MalformedURLException
     */
    public RemoteWebDriver remoteWebDriver(String browser, String version, String platform, ITestContext testContext,String gridHub) throws MalformedURLException, InterruptedException {

        System.out.println();
        System.out.println();
        System.out.println("TEST SUITE: " + testContext.getSuite().getName());
        RemoteWebDriver remoteWebDriver = getRemoteWebDriver(browser, version, platform,gridHub);
        testContext.setAttribute("webDriver", remoteWebDriver);
        return remoteWebDriver;

    }

    /**
     *
     * @param browser
     * @param version
     * @param platform
     * @return
     * @throws MalformedURLException
     */
    private RemoteWebDriver getRemoteWebDriver(String browser, String version, String platform,String gridHub) throws MalformedURLException, InterruptedException {

        if(StringUtils.isEmpty(version)) version = "";
 //       String gridHub = "http://10.214.68.51:4444";
        URL hub = new URL(gridHub + "/wd/hub");
        RemoteWebDriver driver;
        driver = new RemoteWebDriver(hub, getCapabilities(browser, version, platform));
        driver.manage().window().maximize();
        return driver;

    }

    /**
     * Provides a single instance of a the EventFiringWebDriver requiring an augmented WebDriver
     * I.e. Uses the WebDriverEventAdapter for event firing implementations
     * @see WebDriverEventAdapter
     * @return
     * @throws MalformedURLException
     */
    public EventFiringWebDriver eventFiringRemoteWebDriver(WebDriver driver, Long defaultWait) throws MalformedURLException {

        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        WebDriverEventAdapter eventHandler = new WebDriverEventAdapter(driver);
        eventFiringWebDriver.register(eventHandler);
        eventFiringWebDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        eventFiringWebDriver.manage().window().maximize();
        return eventFiringWebDriver;

    }

 /*
     * Used for local automation validation, does not create a driver on the Grid
     * Uses any version available for the given browser provided
      * @param browser chrome, iexplorer, and firefox: default is firefox
     * @param platform the operating system VISTA, XP, WIN7, LINUX
     * @param browserVersion the version of the browser
     * @return
     * */

    public WebDriver webDriver(String browser, String platform, String browserVersion, ITestContext testContext) {

        WebDriver driver;
        browser = browser.toLowerCase();
        File driverServer;

        switch(browser) {
            case "chrome" :
                driverServer = this.getDriverServer("chromedriver");
                if(StringUtils.isBlank(System.getProperty("webdriver.chrome.driver"))) {
                    System.setProperty("webdriver.chrome.driver", driverServer.getAbsolutePath());
                }
                driver = new ChromeDriver(getCapabilities(browser, browserVersion, platform));
                break;
            case "internet explorer" :
                driverServer = this.getDriverServer("IEDriverServer");
                if(StringUtils.isBlank(System.getProperty("webdriver.ie.driver"))) {
                    System.setProperty("webdriver.ie.driver", driverServer.getAbsolutePath());
                 }
                driver = new InternetExplorerDriver(getCapabilities(browser,browserVersion,platform));
                break;
            case "edge" :
                driverServer = this.getDriverServer("MicrosoftWebDriver");
                String path=driverServer.getAbsolutePath();
                System.setProperty("webdriver.edge.driver", path);
                driver = new EdgeDriver(getCapabilities(browser,browserVersion,platform));
                break;
            case "firefox" :
                driverServer = this.getDriverServer("geckodriver");
                String driverLog=driverServer.getAbsolutePath();
                System.setProperty("webdriver.gecko.driver", driverServer.getAbsolutePath());
                driver = new FirefoxDriver(getCapabilities(browser,browserVersion,platform));
                break;
            default:
                driverServer = this.getDriverServer("chromedriver");
                if(StringUtils.isBlank(System.getProperty("webdriver.chrome.driver"))) {
                    System.setProperty("webdriver.chrome.driver", driverServer.getAbsolutePath());
                }
                driver = new ChromeDriver(getCapabilities(browser,browserVersion,platform));
                break;
        }

        driver.manage().window().maximize();
        testContext.setAttribute("webDriver", driver);
        return driver;

    }

    private File getDriverServer(String driverName) {

        String fileExtension = ".exe";  // Set default driver file extension
        // Check to see if current OS is linux (not windows) AND browser request is CHROME then remove ".exe" from the driver server's file extension
        if(driverName.equals("edge") && !System.getProperty("os.name").startsWith("Windows")) {

            fileExtension = "";

        }
        File driverFile;
        String osName=System.getProperty("os.name");
        try {
            if (osName.contains("Linux")){
                driverFile = new File(driverName);}
            else if (osName.contains("Mac") )
            {driverFile = new File(driverName+"_mac");}
            else
            {driverFile = new File(driverName + fileExtension);}
            if(!driverFile.exists()) {
                InputStream driverFileInputStream = TaiteWebDriverConfig.class.getClassLoader().getResourceAsStream("webDrivers/" + driverName + fileExtension);
                OutputStream outputStream = new FileOutputStream(driverFile);
                IOUtils.copy(driverFileInputStream, outputStream);
                outputStream.close();
                return driverFile;

            }
            else {

                if(driverFile.canExecute()) {
                    return driverFile;
                }
                else {
                    driverFile.setExecutable(true);
                    if(driverFile.canExecute()) {
                        return driverFile;
                    }
                    else {
                        throw new RuntimeException("ERROR: Found driverFile for " + driverName + " but it is not executable, tried to setExecutable(true) but failed");
                    }
                }

            }

        }
        catch (IOException e) {
            throw new RuntimeException("ERROR: Failed to find, created, or otherwise initialize the web driver using " + driverName + ", please check to make sure the driver name specified is correct");
        }

    }

    private Platform getPlatform(String browserPlatform) {

        Platform platform = Platform.ANY;
        switch(browserPlatform) {
            case "VISTA" :
                platform = Platform.VISTA;
                break;
            case "WINDOWS" :
                platform = Platform.WINDOWS;
                break;
            case "WIN8" :
                platform = Platform.WIN8;
                break;
            case "LINUX" :
                platform = Platform.LINUX;
                break;
            case "MAC" :
                platform = Platform.MAC;
                break;
            case "XP" :
                platform = Platform.XP;
                break;
            case "WIN10" :
                platform = Platform.WIN10;
            default:
                platform = Platform.VISTA;
                break;
        }
        return platform;

    }

    private DesiredCapabilities getCapabilities(String browser, String version, String platform){

        DesiredCapabilities capabilities;
         switch(browser) {
            case "chrome" :
                Proxy proxy=new Proxy();
                proxy.setProxyType(org.openqa.selenium.Proxy.ProxyType.SYSTEM);
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--ignore-certificate-errors");
                chromeOptions.addArguments("--disable-extensions");
                chromeOptions.addArguments("--disable-dev-tools");
                chromeOptions.addArguments("--disable-web-security");
                capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(CapabilityType.PROXY, proxy);
                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                break;
            case "internet explorer" :
                capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                capabilities.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
                 break;
            case "firefox" :
                capabilities = DesiredCapabilities.firefox();
                break;
            case "edge" :
                capabilities = DesiredCapabilities.edge();
                capabilities.acceptInsecureCerts();
                break;
             case "winappdriver":
                 DesiredCapabilities appCapabilities = new DesiredCapabilities();
                 appCapabilities.setCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
//                 CalculatorSession = new RemoteWebDriver(new Uri(WindowsApplicationDriverUrl), appCapabilities);
//                 Assert.IsNotNull(CalculatorSession);
 //                CalculatorSession.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
// Make sure we're in standard mode
//                 CalculatorSession.FindElementByXPath("//Button[starts-with(@Name, \"Menu\")]").Click();
//                 OriginalCalculatorMode = CalculatorSession.FindElementByXPath("//List[@AutomationId=\"FlyoutNav\"]//ListItem[@IsSelected=\"True\"]").Text;
//                 CalculatorSession.FindElementByXPath("//ListItem[@Name=\"Standard Calculator\"]").Click();
            default:
                capabilities = DesiredCapabilities.chrome();
                break;
        }

//        capabilities.setBrowserName(browser);
//        capabilities.setPlatform(this.getPlatform(platform));
//        capabilities.setVersion(version);
//        capabilities.setJavascriptEnabled(true);
//        capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        return capabilities;

    }

    private void printBrowserConfiguration(String browser, String browserVersion, String platform) {

        System.out.println();
        System.out.println("**** BROWSER CONFIGURATION ****");
        System.out.println("** Browser: " + browser);
        System.out.println("** Version: " + browserVersion);
        System.out.println("** Platform: " + platform);
        System.out.println("*******************************");
        System.out.println();

    }

}
